package com.ynov;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static com.ynov.FizzBuzz.*;
import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
public class FizzBuzzTest {
    private FizzBuzz fizzBuzz;

    @BeforeEach
    public void setup() {
        fizzBuzz = new FizzBuzz();
    }

    @AfterEach
    public void tear() {
        fizzBuzz = null;
    }

    @Test
    public void fizzbuzz_should_print() {
        fizzBuzz.print(200);
    }

    @ParameterizedTest(name = "{0} doit être égal à {0}")
    @ValueSource(ints = {1, 2, 4, 8})
    public void fizzbuzz_should_return_numbers_not_multiple_3_or_5_or_7(int arg) {
        // When
        var result = fizzBuzz.convert(arg);

        // Then
        assertThat(result).isEqualTo(String.valueOf(arg));
    }

    @ParameterizedTest(name = "{0} doit être égal à " + FIZZ)
    @ValueSource(ints = {3, 6, 9})
    public void fizzbuzz_should_return_fizz_with_numbers_multiple_3(int arg) {
        // When
        var result = fizzBuzz.convert(arg);

        // Then
        assertThat(result).isEqualTo(FIZZ);
    }

    @ParameterizedTest(name = "{0} doit être égal à " + BUZZ)
    @ValueSource(ints = {5, 10})
    public void fizzbuzz_should_return_buzz_with_numbers_multiple_5(int arg) {
        // When
        var result = fizzBuzz.convert(arg);

        // Then
        assertThat(result).isEqualTo(BUZZ);
    }

    @ParameterizedTest(name = "{0} doit être égal à " + POP)
    @ValueSource(ints = {7, 14})
    public void fizzbuzz_should_return_pop_with_numbers_multiple_7(int arg) {
        // When
        var result = fizzBuzz.convert(arg);

        // Then
        assertThat(result).isEqualTo(POP);
    }

    @ParameterizedTest(name = "{0} doit être égal à " + FIZZ_BUZZ)
    @ValueSource(ints = {15, 30})
    public void fizzbuzz_should_return_fizzbuzz_with_numbers_multiple_3_and_5(int arg) {
        // When
        var result = fizzBuzz.convert(arg);

        // Then
        assertThat(result).isEqualTo(FIZZ_BUZZ);
    }

    @ParameterizedTest(name = "{0} doit être égal à " + FIZZ_POP)
    @ValueSource(ints = {21, 42})
    public void fizzbuzz_should_return_fizzpop_with_numbers_multiple_3_and_7(int arg) {
        // When
        var result = fizzBuzz.convert(arg);

        // Then
        assertThat(result).isEqualTo(FIZZ_POP);
    }

    @ParameterizedTest(name = "{0} doit être égal à " + BUZZ_POP)
    @ValueSource(ints = {35, 70})
    public void fizzbuzz_should_return_buzzpop_with_numbers_multiple_5_and_7(int arg) {
        // When
        var result = fizzBuzz.convert(arg);

        // Then
        assertThat(result).isEqualTo(BUZZ_POP);
    }

    @ParameterizedTest(name = "{0} doit être égal à " + FIZZ_BUZZ_POP)
    @ValueSource(ints = {105, 210})
    public void fizzbuzz_should_return_fizzbuzzpop_with_numbers_multiple_3_and_5_and_7(int arg) {
        // When
        var result = fizzBuzz.convert(arg);

        // Then
        assertThat(result).isEqualTo(FIZZ_BUZZ_POP);
    }
}
