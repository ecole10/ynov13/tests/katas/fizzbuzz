package com.ynov;

public class FizzBuzz {
    public static final String FIZZ = "Fizz";
    public static final String BUZZ = "Buzz";
    public static final String POP = "Pop";
    public static final String FIZZ_BUZZ = "FizzBuzz";
    public static final String FIZZ_POP = "FizzPop";
    public static final String BUZZ_POP = "BuzzPop";
    public static final String FIZZ_BUZZ_POP = "FizzBuzzPop";

    public void print(int max) {
        for (int i = 1; i <= max; i++) {
            System.out.println(convert(i));
        }
    }

    public String convert(int number) {
        if (isFizzBuzzPop(number)) {
            return FIZZ_BUZZ_POP;
        } else if (isFizzBuzz(number)) {
            return FIZZ_BUZZ;
        } else if (isFizzPop(number)) {
            return FIZZ_POP;
        } else if (isBuzzPop(number)) {
            return BUZZ_POP;
        } else if (isFizz(number)) {
            return FIZZ;
        } else if (isBuzz(number)) {
            return BUZZ;
        } else if (isPop(number)) {
            return POP;
        } else {
            return String.valueOf(number);
        }
    }

    private boolean isFizz(int number) {
        return number % 3 == 0;
    }

    private boolean isBuzz(int number) {
        return number % 5 == 0;
    }

    private boolean isPop(int number) {
        return number % 7 == 0;
    }

    private boolean isFizzBuzz(int number) {
        return isFizz(number) && isBuzz(number);
    }

    private boolean isFizzPop(int number) {
        return isFizz(number) && isPop(number);
    }

    private boolean isBuzzPop(int number) {
        return isBuzz(number) && isPop(number);
    }

    private boolean isFizzBuzzPop(int number) {
        return isFizzBuzz(number) && isPop(number);
    }
}
